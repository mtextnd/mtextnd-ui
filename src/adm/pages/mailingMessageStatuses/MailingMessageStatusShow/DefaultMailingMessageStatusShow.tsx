/* eslint-disable max-len */
import React, {FC} from 'react';
import {
  Show,
  ShowProps,
  TabbedShowLayout,
  useTranslate,
} from 'react-admin';
import MailingMessagesMailingMessageStatusIdTab from './tabs/MailingMessagesMailingMessageStatusIdTab';
import MainTab from './MainTab';
import {additionalTabs} from './additionalTabs';
import DefaultActions from './DefaultActions';

// DO NOT EDIT! THIS IS GENERATED FILE

const DefaultMailingMessageStatusShow: FC<ShowProps> = (props: ShowProps) => {
  const translate = useTranslate();

  return (
    <Show actions={<DefaultActions />} {...props}>
      <TabbedShowLayout>
        <MainTab label={translate('app.mainTab')} />
        {additionalTabs.map(({Tab, label}, i) => <Tab label={label} key={i} />)}
        <MailingMessagesMailingMessageStatusIdTab label={translate('catalogs.mailingMessages.title')} path='mailingMessages-mailingMessageStatusId' />
      </TabbedShowLayout>
    </Show>
  );
};

export default DefaultMailingMessageStatusShow;
