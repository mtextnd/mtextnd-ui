import * as React from 'react';
import {
  Route,
} from 'react-router-dom';
import Loadable from '../shared/Loadable';

const LoadableDashboardStats = Loadable({
  loader: () => import('./DashboardStats'),
});

const additionalRoutes = [
  <Route element={<LoadableDashboardStats />} key='statsDashboard' path='/statsDashboard' />,
];

export default additionalRoutes;
