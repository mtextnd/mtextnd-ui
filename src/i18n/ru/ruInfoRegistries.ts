/* eslint-disable max-len */

// DO NOT EDIT! THIS IS GENERATED FILE

const ruInfoRegistries = {
  aggregateTrackings: {
    title: 'Aggregate Trackings',
    fields: {
      id: 'Ид',
      entityTypeId: 'Тип сущности',
      entityId: 'Сущность',
      lastAggregatesComputed: 'Агрегаты последний раз вычислены ',
      lastEntityUpdate: 'Последнее обновление сущности',
      aggregateVersion: 'Версия агрегата',
    },
  },
};

export default ruInfoRegistries;
