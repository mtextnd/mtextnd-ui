/* eslint-disable max-len */

// DO NOT EDIT! THIS IS GENERATED FILE

const enInfoRegistries = {
  aggregateTrackings: {
    title: 'Aggregate Trackings',
    fields: {
      id: 'Id',
      entityTypeId: 'Тип сущности',
      entityId: 'Сущность',
      lastAggregatesComputed: 'Агрегаты последний раз вычислены ',
      lastEntityUpdate: 'Последнее обновление сущности',
      aggregateVersion: 'Версия агрегата',
    },
  },
};

export default enInfoRegistries;
